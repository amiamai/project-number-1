
var r;
var g;
var b;

let img;
let imgtwo;
function preload() {
  img = loadImage('overskæg.png');
  imgtwo = loadImage('symbols.png');

}

function setup() {
  createCanvas (1000,600);
  background ('#D8BFD8');
frameRate(10);

}




function draw() {

  //last hair - behind emoji
  ellipse(230, 355, 60, 60);
  ellipse(375, 355, 60, 60);

//emoji heads

fill('#FFFF00');
circle(300, 250, 120);

circle(700, 250, 120);


stroke(250,300,700);



//moustage
image(img, 189,254, 225, 70);

//gender symbols
image(imgtwo, 380,390, 300,200);




fill(255);
//Eyes on left emoji
ellipseMode(RADIUS); // Set ellipseMode to RADIUS

ellipse(255, 235, 30, 20); // Draw white ellipse using RADIUS mode

ellipseMode(CENTER); // Set ellipseMode to CENTER

if(frameCount % 2 ==0) {
r=random(0,0);
g=random(0,355);
b=random(0,255);
}
fill(r,g,b);
ellipse(255, 235, 30, 30); // Draw gray ellipse using CENTER mode

//black in eye
fill(0);
ellipse(255, 235, 15, 20);


ellipseMode(RADIUS); // Set ellipseMode to RADIUS

fill(255);
ellipse(345, 235, 30, 20); // Draw white ellipse using RADIUS mode

ellipseMode(CENTER); // Set ellipseMode to CENTER
if(frameCount % 2 ==0) {
r=random(0,0);
g=random(0,355);
b=random(0,255);
}
fill(r,g,b);
ellipse(345, 235, 30, 30); // Draw gray ellipse using CENTER mode

//black in eye
fill(0);
ellipse(345, 235, 15, 20);





fill(255);
//Eyes on right emoji
ellipseMode(RADIUS); // Set ellipseMode to RADIUS

ellipse(655, 235, 30, 20); // Draw white ellipse using RADIUS mode


ellipseMode(CENTER); // Set ellipseMode to CENTER

if(frameCount % 2 ==0) {
r=random(0,0);
g=random(0,300);
b=random(0,255);
}
fill(r,g,b);
ellipse(655, 235, 30, 30); // Draw gray ellipse using CENTER mode

//black in eye
fill(0);
ellipse(655, 235, 15, 20);

ellipseMode(RADIUS); // Set ellipseMode to RADIUS
fill(255);
ellipse(745, 235, 30, 20); // Draw white ellipse using RADIUS mode


ellipseMode(CENTER); // Set ellipseMode to CENTER

if(frameCount % 2 ==0) {
r=random(0,0);
g=random(0,355);
b=random(0,255);
}
fill(r,g,b);
ellipse(745, 235, 30, 30); // Draw gray ellipse using CENTER mode

//black in eye
fill(0);
ellipse(745, 235, 15, 20);


//nose left emoji
fill('#FFD700');
ellipse(300, 270, 20, 10);

//nose right emoji
ellipse(700, 270, 50, 25);



textSize(80);
push();
translate(648,333);
rotate(radians(30));
text('💋 ', 0, 0);
pop();

textSize(70);
text('👄',263, 352);




//Hair right emoji
fill('#A0522D')
push();
translate(660,155);
rotate(radians(160));
ellipse(0, 0, 165, 60);
pop();

push();
translate(750,155);
rotate(radians(25))
ellipse(0, 0, 165, 60);
pop();


//Hair left emoji
fill('	#8B4513');

ellipse(290,138,25,25);
ellipse(307,138,25,25);

ellipse(270, 138, 30, 30);
ellipse(326, 138, 30, 30);

ellipse(244, 147, 35, 35);
ellipse(348, 147, 35, 35);

ellipse(215, 165, 40, 40);
ellipse(378, 165, 40, 40);

ellipse(195, 198, 40, 40);
ellipse(399, 198, 40, 40);

ellipse(190, 238, 45, 45);
ellipse(410, 238, 45, 45);

ellipse(187, 280, 50, 50);
ellipse(415, 280, 50, 50);

ellipse(187, 320, 50, 50);
ellipse(414, 320, 50, 50);

ellipse(190, 355, 55, 55);
ellipse(410, 355, 55, 55);



textSize(210);
text('🎩',595,160);


//eyelash right eye (curveTangent)
push();
rotate(radians(-70));
translate(-160,369);

stroke(100)

noFill();
curve(5, 26, 73, 24, 73, 61, 15, 65);

let steps = 6;
for (let i = 0; i <= steps; i++) {
  let t = i / steps;
  let x = curvePoint(5, 73, 73, 15, t);
  let y = curvePoint(26, 24, 61, 65, t);
  //ellipse(x, y, 5, 5);
  let tx = curveTangent(5, 73, 73, 15, t);
  let ty = curveTangent(26, 24, 61, 65, t);
  let a = atan2(ty, tx);
  a -= PI / 2.0;
  line(x, y, cos(a) * 8 + x, sin(a) * 8 + y);

  fill(100);

}

pop();


//eyelash left eye (curveTangent)
push();
translate(237,307);
rotate(radians(-117));

stroke(100);

push();
noFill();
curve(5, 26, 73, 24, 73, 61, 15, 65);
pop();


let stepstwo = 6;
for (let i = 0; i <= stepstwo; i++) {
  let ttwo = i / steps;
  let xtwo = curvePoint(5, 73, 73, 15, ttwo);
  let ytwo = curvePoint(26, 24, 61, 65, ttwo);
  //ellipse(x, y, 5, 5);
  let txtwo = curveTangent(5, 73, 73, 15, ttwo);
  let tytwo = curveTangent(26, 24, 61, 65, ttwo);
  let atwo = atan2(tytwo, txtwo);
  atwo -= PI / 2.0;

  line(xtwo, ytwo, cos(atwo) * 8 + xtwo, sin(atwo) * 8 + ytwo);



}


pop();


}
