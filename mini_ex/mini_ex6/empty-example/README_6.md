![ScreenShot](Skærmbillede6_1.png)

![ScreenShot](Skærmbillede6_2.png)



https://cdn.staticaly.com/gl/amiamai/project-number-1/raw/master/mini_ex/mini_ex6/empty-example/index.html?fbclid=IwAR2rtAg53DbVSgI50q6yxNAa9EIdtSBrb1net8CAiLjVL7M9chjX9bi5s3E


For this week's mini_ex, I had an idea to make a game that took its standpoint from the "jitters" that Daniel Shiffman made in one of his videos.
I made some changes to this code, gave it color in both the stroke, the ellipses and the background.
Furthermore, I changed the sizes and number of "jitters"or "bugs" as they are called in my code, to make it fit my visions. 
I quickly figured out that I wanted to make an easy object, that wasn't too hard to create and that most people would know.
Hence, I wanted to make an eraser with a happy face, that could erase all of the ellipses with sad faces all over the setting. 
The purpose of this game would then be to erase all of the sad faces in the setting with the “happyface-eraser” within 40 seconds to win the game,
and its hereby a really simple game. 

Moreover, I wanted it to reveal the caption “YOU WIN” or “YOU LOSE” so that the users would know how well or bad they did in their try.
Furthermore, when the user got to win the game the caption “You fought through all of the negativeness – now you can be HAPPY!” should show up. 
Therefore, the intention and morality of my game was meant to be, that you should erase and fight all of the negativeness in your life – 
so that you can be happy. Very cheesy, but kind of funny and a light topic for a game in my opinion. 

I made a jitter class for the jittering bugs and this made the game a lot simpler with the background and stuff, however,
I wasn’t able to make more than 20 bugs or else the program would not work optimally, and it would lag a lot.
I also used the preload function to get some pictures of “sad faces” onto the canvas. 
I had a lot of difficulty getting the sad faces to disappear when the eraser/cloth touched them,
which was a thing that was really important to the whole concept and idea of my game. 

In the end, I didn’t get my mini_ex game to work in the way that I wanted and had imagined.
I used a lot of time getting the sadface-images to disappear when being touched by the eraser/cloth but didn’t succeed.
This caused that my program couldn’t end up being a game as I had intended. 

I think that this week’s task was really difficult, but also really amusing. 
I wish that I could have made my game work as I had intended, but it just wasn’t possible for me, which I really think was a shame. 

- Amia Mai
