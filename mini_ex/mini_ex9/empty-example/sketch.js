// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/0jjeOYMjmDU

var angle = 0;
var slider;

function setup() {
  createCanvas(700, 700);
  slider = createSlider(0, TWO_PI, PI / 10, 0.01);
}

function draw() {
  background('#fae');
  angle = slider.value();
  stroke(135);
  translate(350, height);
  branch(200);

}

function branch(len) {
  line(0, 0, 0, -len);
  translate(0, -len);
  if (len > 4) {
    push();
    rotate(angle);
    branch(len * 0.70);
    pop();
    push();
    rotate(-angle);
    branch(len * 0.70);
    pop();
  }

  //line(0, 0, 0, -len * 0.67);
}
