var slider;
var slidertwo;
var sliderthee;
var sliderfour;

var button;

//webcam
let capture;
let webcamOn = false;

var watchingyou;

//images
 let img;
 let imgtwo;
 function preload() {
   img = loadImage('symbol.png');
  imgtwo = loadImage('sidebar.png');

}

function setup() {
  createCanvas (630,600);
  background ('300');


//sliders
//1
 slider = createSlider(0, 255, 127);
 slider.position(320, 150);
 slider.style('width', '200px');

//2
 slidertwo = createSlider(0, 255, 127);
 slidertwo.position(320, 230);
 slidertwo.style('width', '200px');

//3
 sliderthree = createSlider(0, 255, 127);
 sliderthree.position(320, 365);
 sliderthree.style('width', '200px');

//4
 sliderfour = createSlider(0, 255, 127);
 sliderfour.position(320, 445);
 sliderfour.style('width', '200px');


//button
 button = createButton('Done');
  button.position(487, 520);
  button.size(100,30);
  button.style('background','#3b5998');
  button.style('font-size', '20px');
  button.style('color', 'white');


//webcam
  capture = createCapture();
  capture.size(850,windowHeight);
  capture.position(-200,-80);
  capture.hide();

}

//button
function changeBG() {
  var val = random(255);
  background(val);

}


function draw() {



  if(!webcamOn)

//slider
  var femalemale = slider.value();
    //background(val);

  // slider two
  var catdog= slidertwo.value();

  //slider three
  var womenmen=sliderthree.value();

  //slider four
  var pizzaburger=sliderfour.value();




//top bar
fill('#f7f7f7');
stroke(197);
rect(0, 0, 629, 55);

//side bar
fill(300);
rect(0,55,230,543);



     //symbol picture
     image(img, 8, 10, 70, 37);

    //sidebar picture
     image(imgtwo, 2,80, 200,260);




//headlines
textSize(25);
fill('#3b5998');
text('About you', 250, 100);
fill(0, 102, 253);

textSize(19);
fill('#3b5998');
text('I like:', 252, 320);
fill(0, 102, 153);


//text
textSize(13);
fill(150);
text('Female', 269, 160);
fill(0, 102, 153);

textSize(13);
fill(150);
text('Male', 530, 160);
fill(0, 102, 153);



textSize(13);
fill(150);
text('Dog-person', 247, 240);
fill(0, 102, 153);

textSize(13);
fill(150);
text('Cat-person', 530, 240);
fill(0, 102, 153);



textSize(13);
fill(150);
text('Women', 270, 375);
fill(0, 102, 153);

textSize(13);
fill(150);
text('Men', 530, 375);
fill(0, 102, 153);



textSize(13);
fill(150);
text('Pizza', 277, 455);
fill(0, 102, 153);

textSize(13);
fill(150);
text('Burger', 530, 455);
fill(0, 102, 153);



//new colors on text

if(femalemale!==127) {

textSize(13);
fill(200,25,25,200-femalemale);
text('Female', 269, 160);
fill(0, 102, 153);

textSize(13);
fill(59,89,152,femalemale);
text('Male', 530, 160);
fill(0, 102, 153);

}


if(catdog!==127){

  textSize(13);
  fill(200,25,25,255-catdog);
  text('Dog-person', 247, 240);
  fill(0, 102, 153);

  textSize(13);
  fill(59,89,152,catdog);
  text('Cat-person', 530, 240);
  fill(0, 102, 153);
}

if(womenmen!==127){

textSize(13);
fill(200,25,25,255-womenmen);
text('Women', 270, 375);
fill(0, 102, 153);

textSize(13);
fill(59,89,152,womenmen);
text('Men', 530, 375);
fill(0, 102, 153);
}

if(pizzaburger!==127){

textSize(13);
fill(200,25,25,255-pizzaburger);
text('Pizza', 277, 455);
fill(0, 102, 153);

textSize(13);
fill(59,89,152,pizzaburger);
text('Burger', 530, 455);
fill(0, 102, 153);
}

  button.mousePressed(webcam);

}

function webcam() {



background('#3b5998');


webcamOn = true;
capture.show();
button.hide();



watchingyou=createDiv('You are now your own personalized human being!')
watchingyou.position(8,560);
watchingyou.style('font-size', '31px');
watchingyou.style('color', 'yellow');

watchingyou=createDiv('CONGRATULATIONS')
watchingyou.position(190,530);
watchingyou.style('font-size', '31px');
watchingyou.style('color', 'yellow');




watchingyou=createDiv('👍')
watchingyou.position(60,400);
watchingyou.style('font-size', '60px');

watchingyou=createDiv('👍')
watchingyou.position(40,250);
watchingyou.style('font-size', '50px');

watchingyou=createDiv('👍')
watchingyou.position(80,40);
watchingyou.style('font-size', '80px');

watchingyou=createDiv('👍')
watchingyou.position(300,30);
watchingyou.style('font-size', '40px');

watchingyou=createDiv('👍')
watchingyou.position(550,200);
watchingyou.style('font-size', '60px');

watchingyou=createDiv('👍')
watchingyou.position(450,50);
watchingyou.style('font-size', '80px');

watchingyou=createDiv('👍')
watchingyou.position(540,400);
watchingyou.style('font-size', '50px');



}
