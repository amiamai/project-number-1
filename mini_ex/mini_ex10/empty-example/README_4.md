![ScreenShot](Skærmbillede4_1.png)

![ScreenShot](Skærmbillede4_2.png)

![ScreenShot](Skærmbillede4_3.png)


https://gl.githack.com/amiamai/project-number-1/raw/master/mini_ex/mini_ex4/empty-example/index.html



My first thoughts on this week’s mini_ex was that I wanted to make it possible for people have more opportunities and to make more personalized choises
beyond the well-known checkboxes. Hence, I came up with the idea of using slide-bars instead of only two separate boxes,
when for example going on Facebook and having to choose from the possibilities man or woman, when setting up your profile. 

In this program, I have been using functions to make both sliders, a button and at last the use of the webcam.
Furthermore, I been using rectangles and pictures, to make the program seem more like the well-known media, Facebook.
At last, I have put in text on each side of the sliders and made it change between the colors red and blue
whenever you move the slider to one side or the other side, by using if-statements.

In the sense of “capturing all”, my program makes it possible to not just choose from two boxes of categories,
but instead being able to put a slider in exactly the place, where you as a person feel that you belong the most.
I made my program as a Facebook “edit your profile” page, because of the examples in last week’s class,
where this media was used as an example of not really having that many choices when setting up a profile. 
Hereby, I made some changes, that I feel could make a little bit of change in this field. 
Perhaps, it could make some more people feel that they have more possibilities to choose from,
and possibly even make some fell a bit more included, without being put in a specific labeled box. 
This program is indented to “capture all” in the way, that it gives people more possibilities to choose from
and therefore giving them the chance to be their own personalized human being.

We live in a society filled with the need of putting people in specific boxes with specific labels,
and maybe this could be a small step towards breaking down these boxes and for people to be just as the want to be. 

- Amia Mai
