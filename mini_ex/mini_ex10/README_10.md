![Screenshot](Skærmbillede10_1.png)

*See Mini_ex4, that this flowchart is made from, by entering the mini_ex10 and the empty example folder.*


The difficulty in making this flowchart was making all of the possible choices visable. 
This flowchart might seem simple and I would say that the program is also somewhat simple, however the program consists of a bunch of choices, that was really hard to show in the flowchart.
In the program you can choose to interact with four sliders, that all have at least three possible choises each; Sliding it to the right, to the left or not sliding it at all. 
However, you could say that there also is a possibility to set the sliders in between one of these "settings", which makes it have even more possiblities. 
Hence, the difficulty in making this program into a flowchart lies in the almost endless possibilities in the settings of the four sliders, and by the mean of this, I chose to simplify the flowshart. 
This choise makes the flowchart seem rather simple, but I couldn't really see another way in making this, without having to make hundreds of boxes to fit in the almost endless possibilities. 

![Screenshot](Flowchart_1.jpg)  ![Screenshot](Flowchart_2.jpg)

These flowcharts that we made in the group are different from my own flowchart in the way that the process was the other way around. 
In my own flowchart I made a program beforehand and boiled the interaction choices down to a flowchart, however, the flowcharts we made in the group were based on ideas of programs we thought would be interesting to execute. 
Hence, the first flowchart was the use of a pre-made program, while the other two only showed kind of a draft over an idea that could become a lot more complex, when being made. 



**Yellow flowchart - The Walled Garden**

The idea that the yellow flowchart came from was Facebook or some other big media being like a “walled garden”.
Hereby you would have to accept some extreme terms and conditions to get into this garden and to be accepted. 
This idea works with data capturing and it has a political side in you as a user accepting terms and conditions without even knowing what you are agreeing to.  

**Purple flowchart - Ranked Donations**

The purple flowchart revolves around the idea of donations being some kind of social ranking system of how good of a person you are.
Our inspiration came from a new Facebook feature that allows you to start a fundraising for a charity that you choose yourself, that people then can donate to on your birthday. 
We found this this new Facebook feature really nice, however it could also create some social issues and questions; because are you a bad person if you don´t do this on your birthday then? 
This led us to the idea behind the flowchart, which starts with a reminder that tells you to remember to donate.
If you forget to donate money to charity or choose not to, you lose points and your ranking drops – you are now a worse person.
However, if you choose to donate money you receive points based on the amount of money you donated, and your donations will be shared on all social medias. Hereby, all of your friends will see how good of a person you are.  
This will most likely be the program that we focus on for our final project.

- Amia Mai 