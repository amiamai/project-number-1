![Screenshot](Skærmbillede3_1.png)

![Screenshot](Skærmbillede3_2.png)

![Screenshot](Skærmbillede3_3.png)


https://gl.githack.com/amiamai/project-number-1/raw/master/mini_ex/mini_ex3/empty-example/index.html


My first thoughts about this throbber design was, that I wanted to make something out of the 3D figures in the p5 library, 
that together could symbolize and show that time passes. This I wanted to execute by using the torus() syntax as “the star of the show”
and then using the box() syntax to make a smaller box circle around itself in a faster speed inside the toruses.
I wanted this mini_ex to be quite simple, but still interesting to look at. To make the program a bit more fascinating to look at,
I added a lot of different colors to go with the shapes, both in the filling of the figures and in the stroke. 
Furthermore, I only used the function draw() itself to make my program loop, as it itself is a loop function.
This loop, I discovered could make the toruses bump into each other and make them change colors, which I found very hypnotizing to look at.
I find that this program does symbolize some kind of time passing by the different movements and change of colors,
and it does in a way make your mind relax a bit, instead of teasing it - but it really isn’t a ‘traditional’ throbber. 
In the end, this is a really, really simple program, and maybe also a bit too simple. 

I tried to put in some a syntax that I found in the p5 library by the name minute() but unfortunately without any luck.
I also tried to use the function of putting in emojis, in which I wanted to use the “hourglass” emoji as the thing in the middle of the toruses,
to make a symbol that could be related to time passing inside of these toruses – but this I also didn’t succeed with. 
All in all I am not really completely satisfied with my outcome in this week’s mini_ex,
and I would have liked to explore a lot of other syntaxes and functions, but I couldn’t really make all of the things work optimally together 
– and therefore it ended up like this. 

I would really like in my future mini_ex’es to study more of the syntaxes and functions and make more interesting codes.
Hence, I think that I will try and make this my goal for the next mini ex. 

I think, that the thing that people generally think most about, when stubbing into a throbber, is that the program is working on your command.
This was also what I thought about the throbber, before reading this weeks text, which gave me a better look into the throbber’s nature and function today.
Some big webpages like Facebook use the throbber to make the users wait for a bit to make them trust the program more, 
even though the command has already been executed. It seems that people doesn’t trust a program if it’s too fast. 
This I thought was really interesting. 

- Amia Mai
