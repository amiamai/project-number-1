let noiseScale=0.08;

let x = 0;
let y = 0;
let spacing = 0;

var r;
var g;
var b;


function setup () {
  createCanvas(800,800);
  background(0);
}

function draw () {

//lines of light
stroke (r,g,b);
if(frameCount % 1 ==0) {
r=random(0,1000);
g=random(0,1000);
b=random(0,1000);
}

(r,g,b);

strokeWeight(2);

if (random(1) <0.5) {

  line(y,x,y+spacing,y+spacing);
} else {

  line(y,x+spacing,x+spacing,y);
}
y+=8;
if (y > height) {
  y = 0;
  y += spacing;
}


//the buildings changing color
//i did not to go with this one - it was to "wild"

// fill(r,g,b)
//  // stroke (r,g,b);
//  if(frameCount % 15 ==0) {
//  r=random(0,1000);
//  g=random(0,1000);
//  b=random(0,1000);
// }


//the buildings showing - the noise
fill('#FE2E64')
for (let x=0; x < width; x++) {
  let noiseVal = noise((mouseX+x)*noiseScale, mouseY*noiseScale);
  stroke(noiseVal*255);
  rect(x, mouseY+noiseVal*800, x, height);

}


}
