![Screenshot](Skærmbillede7_1.png)

![Screenshot](Skærmbillede7_2.png)

![Screenshot](Skærmbillede7_3.png)

https://gl.githack.com/amiamai/project-number-1/raw/master/mini_ex/mini_ex7/empty-example/index.html


My thoughts on this week’s mini_ex, was that I wanted to make something that was really fascinating to look at.
I started by looking at the experimentation of the 10 print that we had a look at in Tuesday’s class. 
Hereby, I played around with it, to see what I was able to do with it by changing a few parameters and so on.
After doing this for a while, I found that by changing the x and y parameters to the opposite of what they were at the beginning,
you would get random lines drawing a really nice pattern on the screen. 
It would keep on drawing these lines, until the pattern was complete, and then it would just stop. 
I was very intrigued by this pattern, however, I was a bit disappointed that it would just stop after filling in the last line,
so I decided to try and make it in random colors, to see, if this would make any difference – and it did!
Even after this pattern was complete, the program would continuously draw the lines in different colors and hereby make some kind of loop,
even though the lines were complete.
I was really satisfied with this, however, I found that the bottom half of the canvas was a bit dull to look at,
because of the lines only filling up the top half. 
Therefore, I decided to put in a bit more code, and make use of the syntax noise() from the p5 library,
to make something interesting happen at the bottom half of the canvas. 

The rules for my program are:
1.	Create two lines – one vertical, one obliquely.
2.	Do a loop of random colors in these lines.
3.	Create “buildings” that moves with the mouse, with the use of the noise() syntax

After having used the noise() syntax in my code and changing it a bit, the bottom half of my code started to look like some kind of buildings being created
when moving the mouse. Hereby I found, that my program, at least in my mind, look a bit like some kind of skyline,  
and the different colored lines looked a bit like strings of light, when moving the “buildings” up and down.
Therefore, to me, my program has bit of a resemblances to the party town Vegas, or at least that is what pops into my mind,
when interacting with the program. 

Hereby, this week, I took my starting point in the 10 print and ended up with something totally different.
In the end, it is also a really busy program that wants many things at the same time, instead of just being clean, 
simple and hypnotizing as most automatisms and random generated programs are. 


- Amia Mai
