![Screenshot](Skærmbillede1.png)
![Screenshot](Skærmbillede2.png)


https://cdn.staticaly.com/gl/amiamai/project-number-1/raw/master/mini_ex/mini_ex2/empty-example/index.html

https://gl.githack.com/amiamai/project-number-1/raw/master/mini_ex/mini_ex2/empty-example/index.html

https://cdn.staticaly.com/gl/amiamai/project-number-1/raw/master/mini_ex/mini_ex2/empty-example/sketch.js


From this week’s assigned reading, I have learned that such simple things as emojis can have a big impact on a lot of people,
and that it even can be the start of huge discussions. 
Emojis is something that overwhelmingly many people use on an every day basis,which makes it something that they want to be able to identify with. 
On that note, I tried to make two emojis that could somewhat symbolize “the fluid genders” 
and make the people that don’t identify as either a man or a woman, feel more included. 

When I started this mini_ex I had a vision about making two emojis that wasn’t to be found in the library of emojis.
After that, I narrowed my vision down and decided to make a ”woman” with a moustache and a “man” with lipstick on the lips.
To complete this image and this whole idea, I wanted to find a combined man- and woman symbol to symbolize and make some kind of a tribute
to the fluid genders. 

The mixi_ex2 ended out as I had envisioned with two yellow emojis.
One with long hair, eyelashes a small nose, lips and of course a big moustache 
and the other one with short hair, a bigger nose, a hat and lips with red lipstick on. 
To add a bit of something fun, I made the emojis eyes change in a lot of different colors, 
also to in a way symbolize the diversity and make even more people able to identify with these two figures. 
Lastly, I put the unified gender-sign in the middle under the emojis. 

The syntaxes that I focused on in this mini_ex was mostly shapes and how these could be handled.
Also, I used the text-incorporator and the loadImage function to put in the mouths, the hat and the gender-sign.
Lastly, I re-used the random function again, to make the eyes change colors. 
However, this time, I used some variables and some adjustment to the frameCount 
to make the changing of the colors go a bit slower than last time, so that it wouldn’t just be a crazy disco show. 

This week I have learned a lot about the shapes, how to place and rotate them right, 
and how to make them into something other than just abstract “art pieces”. 
I was really impressed about how many things you are able to produce by the codes, 
and how you were able to have an outcome that other people might be able to both understand and, in some way, identify with. 

- Amia Mai
