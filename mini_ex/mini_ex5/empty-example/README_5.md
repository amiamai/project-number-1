![ScreenShot](Skærmbillede5_1.png)
![ScreenShot](Skærmbillede5_2.png)

https://gl.githack.com/amiamai/project-number-1/raw/master/mini_ex/mini_ex5/empty-example/index.html


My thoughts on this mini_ex was to remake my first program and making it a lot more interesting
by using some of the new syntaxes that I have learned while still keeping the program in the same spirit
as the original one. 
 
I have made a heart by two ellipses and a square that I rotated upside down. 
This square is the now center of attention, as the thee ellipses were before. 
Furthermore, this heart is also changing it's color by the use of a random setting as I also used before,
however, I did make some changes to make it change colors a bit slower, to make the whole program a bit more gentle on the eyes.
I also added a button with the caption "PRESS FOR LOVE", which I a first wanted to make small random lovable sentences,
when pressed – but I ended up changing my mind about that, so that the program still could keep some of the same elements it had before. 
Lastly, I changed the mouseX, mouseY function, so that the text "LOVE" would follow the mouse instead of the ellipse that followed it before
-this made it all a bit more interesting in my opinion, and it kind of completed the whole theme that I have set up. 

My first mini_ex didn’t really have any purpose or even some greater thinking behind it all,
because I was so new to the world of programming and coding, but now, I put in some bit of meaning into something
that before didn’t contain anything more than just a bit of visual craziness. 
The theme of this remade mini_ex has been hearts, love and just a kindhearted warm feeling.
It still doesn’t say that much and it certainly isn’t a lifechanging program, but I still think, that it could make some people smile a bit
– and that is good enough for me. 
You can interpret this program in your own way, and that is also some of the meaning behind it.
However, the way that I see it, is that you start seeing this heart flashing in different colors
and the button under it, with the words “PRESS FOR LOVE”. This button invites you to press it,
and it’s tempting nature makes you press it, but before you even get to press it expecting to “get some love”,
the mouse draws out the word “LOVE” and even before you made the mouse press the “love button”, you already got it. 
This could in a way symbolize, that you don’t need love, because you’ve already got it. 
So, this button is useless, because there will always be love.

Hence, I achieved to make a useless and uninspiring program into something more interesting to look at
and additionally something with a meaning behind it, which was what I aspired
as the outcome in this remake of a mini_ex. 

- Amia Mai 

