// Collaboration between Cecilie Julie Christensen and Amia Mai Nielsen 

![Screenshot](Skærmbillede8_1.png)

![Screenshot](Skærmbillede8_2.png)


https://gl.githack.com/amiamai/project-number-1/raw/master/mini_ex/mini_ex8/empty-example/index.html


I have been collaborating with Cecilie J. on this mini_ex. We choose to take a starting point in the presentation on Tuesday by Group 6 where they did a JSON-example.
We tried for some time to make the JSON file work, but without much luck. 
Therefore, we choose to move on with the mini_ex without the use of the JSON file and just with the use of variables instead. 

The program starts by showing a pink canvas with black typewriter words of different flower-names that moves all over it.
In the middle of it all is an emoji-symbol of a beautiful tulip and the sound of “Waltz of the flowers” by Tchaikovsky is playing in the background. 
When pressing the mouse, the background shifts from pink to green and names of plants is now moving. There is at this point a plant emoji in the middle
and in the background a sound clip of a YouTube-video is playing. When the mouse is released,
the program turns back to its original state with the “flower background”.

This is the link https://www.youtube.com/watch?v=S5xau561fv4&feature=youtu.be – This is a video of the program in use, the way that it was supposed to work.
This was made because of some difficulty with running the program in different servers, as it didn’t run the sound the way it was supposed to. 

In the program we used both sound, pictures and maybe the most important, when thinking about the assignment for this week,
is variables with arrays, to put in all of the different names for both the flowers and the plants. 
Moreover, we used the mouseIsPressed and the mouseReleased functions, to make the program switch state from flowers to plants
and also to make the sound switch between the two. 
Furthermore, we used for loops to get the words to appear all over the canvas and we put in a “typewriter” font, 
to make the words a bit different in style. 
However, we were a bit confused about how we actually got the text to move, as we only changed a few parameters in the loops to get this outcome.


What is the aesthetic aspect of your program in particular to the relationship between code and language?
Language and in particular “words” were used in our code as some kind of moving background which matched the themes of the two different “states” of our program.
In the code we made some changes to make the words switch place, which resulted in a flower and plant themed program with
moving plant-names in the background, as well as flower and plant themed sound in both of the “states” of the program. 
In this mini_ex, the language and code in a collaborate process binds the whole program together by emphasizing the through themes.  
When talking about language and code, I think that it has been really interesting to use these in collaboration with each other,
even though we couldn’t get our JSON file to work. Furthermore, it was nice to try and work with different languages in one program, and having this as the task,
to make these two collaborate. Even though programming language and “human language” are really different from each other at first eyesight
it also has its similarities. Learning programming language isn’t that different from beginning to learn a new language as German, Italian ect.
as you in both of these have to learn the basics of “commands” and setup of sentences to be able to communicate. 



This is the link to the full YouTube video that was playing as the sound of the “plant state” of the program:  
https://www.youtube.com/watch?v=L0cOEuLbLrc&t=7s
You can check it out if you LOVE plants and are interested in “how to not kill your plants” ;)

- Amia Mai

