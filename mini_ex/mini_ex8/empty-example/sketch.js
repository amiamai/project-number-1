let data;

function preload(){
  data = loadJSON("plants.json");

}

function setup(){
  createCanvas(1000,400);

function draw(){
  let flower = data.plants[0].family[1];
  text(flower,100,100);
  let plant=data.plants[1].family[3];
  text(plant,200,100);
  let cannabis=data.plants[2].family[1];
  text(cannabis,100,200);
}
