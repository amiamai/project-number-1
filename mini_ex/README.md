![Screenshot](Skærmbillede 1_1.png)
![Screenshot](Skærmbillede 1_2.png)
![Screenshot](Skærmbillede 1_3.png)

https://cdn.staticaly.com/gl/amiamai/project-number-1/raw/master/mini_ex/empty-example/sketch.js


I have chosen to focus on the syntaxes ellipse and fill. 
These I have now learned more about by trying to change the numbers and seeing the differences that these changes made. 
I also tried to switch the numbers out with (mouseX, mouseY) to try and experiment and thereby make one of the ellipses follow the mouse. 

To me the first independent coding process has been really confusing and also a bit frustrating. 
At first, I tried to make some simple ellipses on a neutral background and I experimented with some other 2D shapes as well. 
I moved tried moving these around to make some kind of abstract figure. At last I got the 3 ellipses to stay in the middle of the canvas.
Furthermore, I wanted to switch the colors up, to make this “abstract piece of art” a bit more interesting.
Therefore, I set the three ellipses to light up in all of the rainbow’s colors really fast
and put the background to be a light pink relaxing color, to create a bit more calm in all of the crazy. 
The last thing that I decided to do, was to make an ellipse that was going to move with the mouse while still having all of the crazy colors. 

Therefore, this mini_ex ended up being a light pink background with three crazy ellipses in the middle
and of course the blinking ellipse that follows the mouse and makes the background in crazy circles as well. 
This whole thing ended up consisting of a few syntaxes, that together made something that is not really useful,
but in some way interesting to look at and play with. 

This whole coding process is somewhat similar to reading and writing text, but to me at this early state,
coding is a lot harder than the regular reading and writing. 
But I can imagine that, when you learn more and become more familiar with the whole process of coding and programming,
it will have many similarities to reading and writing text. All this process needs, is time. 

- Amia Mai
